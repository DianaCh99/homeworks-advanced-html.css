/* menu-burger */
const burgerMenu = document.querySelector(".burger-menu");
const headerList = document.querySelector('.header__list--hiden');

burgerMenu.addEventListener("click", clickFunction);

function clickFunction() {
    burgerMenu.classList.toggle('open');
    if (burgerMenu.classList.contains('open')) {
        headerList.style.display = 'block';
    }
    else {
        headerList.style.display = 'none';
    }
}
window.addEventListener('resize', function() {
    if (burgerMenu.classList.contains('open')) {
      headerList.style.display = 'none';
      burgerMenu.classList.remove('open');
    }
  });